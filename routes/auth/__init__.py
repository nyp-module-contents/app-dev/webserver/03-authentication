"""
The routes module contains stuff that are used for debugging.
Usually you don't include in production or deployment
"""
import flask
from   utils.flask import logger
from   data.users  import Users, User, UserRole
#	This is a child route, so we don't specify url_prefix like the previous one
router = flask.Blueprint("auth", __name__)

@router.before_app_request
def inject_user():
    logger().info("Before incoming request")
    if "userid" in flask.session:
        user = Users.select(flask.session["userid"])
        assert (user is not None)
        flask.g.user = user

@router.get("/login")
def page_login():
    return flask.render_template("auth/login.html")


@router.get("/register")
def page_register():
    return flask.render_template("auth/register.html")


@router.get("/logout")
def api_logout():
    flask.session.clear()
    # if "token" in flask.session:
    #     flask.session.pop("token")
    return flask.redirect("/")


@router.get("/test")
def api_test():
    import json
    return json.dumps(list(flask.session.items()))

@router.post("/api/login")
def api_login():
    #try:
        name     = flask.request.form.get("name")
        password = flask.request.form.get("password")

        from data.users import Users
        user = Users.select_name(name)

        if user is None:
            return flask.abort(401)

        #   Correct password
        if user.check_password(password):
            flask.current_app.logger.info("Login Success")

            flask.session["token"]  = "somestuff"
            flask.session["userid"] = user.get_id()
            flask.session["name"]   = user.get_name()
            # flask.session["role"]   = user.get_role()
            #   Userid
            #   Username
            #   entire User
            return flask.redirect("/")
        #   Wrong password, Retry
        else:
            flask.current_app.logger.info(f"Incorrect password : {user.get_password()} {password}")
            return flask.redirect("/auth/login")
    #except Exception as e:
    #    flask.current_app.logger.info(f"Error {e}")
    #    return flask.abort(401)


@router.post("/api/register")
def api_register():
    try:
        name     = flask.request.form.get("name")
        password = flask.request.form.get("password")

        users = Users.list()
        for u in users:
            if u.get_name() == name:
                return flask.Response(response="This user name cannot be used", status=409)

        #   By now it doesn't exists
        Users.insert(User(role=UserRole.USER, name=name, password=password))

        return flask.redirect("/auth/login")

    except Exception as e:
        flask.current_app.logger.error(f"Error occurred {e}")
        return flask.abort(400)