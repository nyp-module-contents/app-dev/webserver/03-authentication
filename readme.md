# Barebone Starter Package

## Purpose

The purpose of this example is to show how to setup a basic Web Flask application along with 
the use of basic jinja template engine. This example uses Bootstrap 4 with Material Design Bootstrap.

## Examples

All standalone examples would be placed in the `/example` route.

## What's next?

Subsequent demonstrations will be done based on this framework

## Getting Started

### Virtual Environment

#### Computer restrictions

Windows users, if you haven't already done this, please set `Set-ExecutionPolicy` to `unrestricted` in your powershell with administrator rights

```powershell
Set-ExecutionPolicy unrestricted
```

#### Create Environment

Open up a terminal in your project and create a `venv` if it doesn't exists. Do not share this folder with your friends and its very fat.

```powershell
python -m venv venv
```

#### Activating Environment

Run the following in windows:

```powershell
./venv/Scripts/activate
```

For Mac/Linux:

```bash
source ./venv/bin/activate
```

### Install packages

Install packages using the following command:

```powershell
pip install <package name>
```

or install via `requirements.txt`

```powershell
pip install -r requirements.txt
```

## Running Server

The following command runs the server and serves at localhost

```powershell
python -m server
```
