import uuid
import enum
import shelve

from   hashlib import sha256

# User ENUM for CONSTANTS or Things that 4ever don't change
class UserRole(enum.IntEnum):
    ADMIN = 0
    USER  = 1

class User:
    def __init__(self, role: UserRole, name: str, password: str, uid: str = None):
        if uid is None:
            self.__id   = str(uuid.uuid4())
        else:
            self.__id   = uid

        self.__role     = role
        self.__name     = name
        self.__password = sha256(password.encode("utf8")).hexdigest()
        #   TODO:
        #   Your own stuff
        #   profile_picture
        #   address
        #   contact
        #   email

    def get_id(self) -> str:
        return self.__id

    def get_password(self) -> str:
        return self.__password

    def set_password(self, password: str):
        self.__password = sha256(password.encode("utf8")).hexdigest()

    def check_password(self, password: str):
        return self.__password == sha256(password.encode("utf8")).hexdigest()

    def set_name(self, name: str):
        self.__name = name

    def get_name(self) -> str:
        return self.__name

    def get_role(self) -> UserRole:
        return self.__role

#  Alternative
#  Create a class Admin and Customer
class Users:
    @staticmethod
    def select(uid: str) -> User:
        with shelve.open("data", "c") as data:
            table = data.get("users", {})
            if uid in table:
                return table[uid]
            else:
                raise KeyError("User not found")

    @staticmethod
    def select_name(name: str) -> User:
        users = Users.list()
        for u in users:
            if u.get_name() == name:
                return u
        return None


    @staticmethod
    def contains(uid: str) -> bool:
        with shelve.open("data", "c") as data:
            table = data.get("users", {})
            if uid in table:
                return True
            else:
                return False

    @staticmethod
    def update(user: User) -> User:
        with shelve.open("data", "c") as data:
            table = data.get("users", {})
            if user.get_id() in table:
                table[user.get_id()] = user
                data["users"] = table
                return user
            else:
                raise KeyError("User don't exists")

    @staticmethod
    def insert(user: User) -> User:
        with shelve.open("data", "c") as data:
            table = data.get("users", {})
            if not user.get_id() in table:
                table[user.get_id()] = user
                data["users"] = table
                return user
            else:
                raise KeyError("User already exists")

    @staticmethod
    def delete(uid: str):
        with shelve.open("data", "c") as data:
            table = data.get("users", {})
            if uid in table:
                del table[uid]
                data["users"] = table
            else:
                raise KeyError("User don't even exists")

    @staticmethod
    def list() -> list:
        with shelve.open("data", "c") as data:
            table = data.get("users", {})
            return table.values()


