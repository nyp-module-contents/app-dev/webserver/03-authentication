import uuid
import shelve

class Product:
    def __init__(self):
        self.uuid        = str(uuid.uuid4())
        self.name        = ""
        self.price       = ""
        self.description = ""

#   DAO - Data Access Object (You want to be familiar if you like Android)
class Products:

    @staticmethod
    def select(id: str):
        with shelve.open("data", "c") as data:
            contents = data.get("products", {})
            if id in contents:
                return contents[id]
            else:
                return None

    @staticmethod
    def insert(p: Product):
        data     = shelve.open("data", "c")
        contents = data.get("products", {})
        contents[p.uuid] = p
        data["products"] = contents
        data.close()

    @staticmethod
    def list() -> [Product]:
        data = shelve.open("data", "c")
        contents = data.get("products", {})
        data.close()
        return contents.values()

    @staticmethod
    def delete(uuid: str):
        data           = shelve.open("data", "c")
        contents: dict = data.get("products", {})
        if uuid in contents:
            contents.pop(uuid)
        data["products"] = contents